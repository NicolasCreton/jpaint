/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jna;

import abstractClass.AbstractManipulationTool;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import utils.PngFilter;
import view.DrawSpace;

/**
 * Classe permettant d'exporter les images en coupant les couleures voulues
 * La zone de dessin est enregistré en bufferedImage
 * La bufferedImage est traduite en tableau de Byte qui est ensuite traité par les fonctions natives
 * Ce tableau de Byte est ensuite retransformé en image et enregistré dans l'endroit voulu par l'utilisateur
 * 
 */
public class JnaExporter extends AbstractManipulationTool{
    private DrawSpace drawSpace;

    public JnaExporter(DrawSpace drawSpace, String colorToCut) {
        this.drawSpace = drawSpace;
        export(colorToCut);
    }
    
    public void export(String colorToCut){
        com.sun.jna.Pointer bufferMemory;
        int[] pixelsArray;

        Graphics2D drawAreaGraphics = (Graphics2D) drawSpace.getG2d();

        //Définitions de la taille de notre image
        int w = (int) Math.ceil(drawAreaGraphics.getClipBounds().getWidth());
        int h = (int) Math.ceil(drawAreaGraphics.getClipBounds().getHeight());

        try {
            
            BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
            Graphics g2 = bufferedImage.getGraphics();
            drawSpace.printAll(g2);

            //Définissons la mémoire allouée pour notre pointeur
            bufferMemory = new com.sun.jna.Memory(w * h * 4);

            //Tableau de Int de taille de l'image
            pixelsArray = new int[w * h];

            //Un raster est un tableau rectangulaire comprenant les pixels de l'images
            Raster raster = bufferedImage.getData();

            //Nous allons alors effectuer les shifts des pixels de l'image afin de récuperer la bonne composante en ARGB et enregistrer le résultat dans le tableau précedent
            int[] pixelArray = new int[4];
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    raster.getPixel(x, y, pixelArray);
                    int red = (pixelArray[2] & 0xFF);
                    int green = (pixelArray[1] & 0xFF) << 8;
                    int blue = (pixelArray[0] & 0xFF) << 16;
                    int alpha = (pixelArray[3] & 0xFF) << 24;
                    pixelsArray[y * w + x] = alpha | red | green | blue;
                }
            }
            
            //Mettons le résultat dans un buffer
            bufferMemory.write(0, pixelsArray, 0, pixelsArray.length);
            
            //Si nous voulons récupérer une couleur, il nous suffit de couper les deux autres composantes
            switch (colorToCut) {
                case "red":
                    Jna.INSTANCE.cutARGBGreen(bufferMemory, w, h);
                    Jna.INSTANCE.cutARGBBlue(bufferMemory, w, h);
                    break;
                case "green":
                    Jna.INSTANCE.cutARGBRed(bufferMemory, w, h);
                    Jna.INSTANCE.cutARGBBlue(bufferMemory, w, h);
                    break;
                case "blue":
                    Jna.INSTANCE.cutARGBGreen(bufferMemory, w, h);
                    Jna.INSTANCE.cutARGBRed(bufferMemory, w, h);
                    break;
                case "mirror":
                    Jna.INSTANCE.mirror(bufferMemory, w, h);
                    break;
                case "original":
                    //On ne fait rien si on veut exporter l'image originale
                    break;
            }

            byte[] bufferOut;

            bufferOut = bufferMemory.getByteArray(0, pixelsArray.length * 4);
            
            //Mettons alors ce buffer de sortie en tableau de Byte
            byte[] imageByte = bufferOut;

            DataBufferByte dataBufferByte = new DataBufferByte(imageByte, imageByte.length);
            
            //Ainsi nous pouvons créer un WritableRaster, c'est à dire un raster avec possibilé d'écriture des pixels pour notre image
            WritableRaster writableRaster = Raster.createInterleavedRaster(dataBufferByte, w, h, w * 4, 4, new int[]{2, 1, 0, 3}, null);
            
            //Définissons l'espace de couleur pour notre nouvelle image créée
            ColorSpace colorSpace = ColorSpace.getInstance(ColorSpace.CS_sRGB);
            int[] bytesArray = {8, 8, 8, 8};
            ColorModel colorModel = new ComponentColorModel(colorSpace, bytesArray, true, false,Transparency.TRANSLUCENT,DataBuffer.TYPE_BYTE);
            
            //Nous pouvons alors mettre construire une BufferedImage grace à ce raster
            BufferedImage cuttedImage = new BufferedImage(colorModel, writableRaster, false, null);
            
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setApproveButtonText("Save");
            fileChooser.setDialogTitle("Export file");
            fileChooser.setFileFilter(new PngFilter());
            fileChooser.setAcceptAllFileFilterUsed(false);
            
            int returnVal = fileChooser.showOpenDialog(drawSpace);
            
            //Enregistrement de l'image dans le dossier choisi au format PNG
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                ImageIO.write(cuttedImage, "PNG", new File(fileChooser.getSelectedFile().getAbsolutePath()+".png"));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
