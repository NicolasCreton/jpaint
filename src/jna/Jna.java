package jna;

import com.sun.jna.Native;
import com.sun.jna.Pointer;

/**
 * 
 * Interface permettant de définir les fonctions natives rédigées en C, elles pourront ainsi etre utilisées ensuite dans le controleur d'exportation d'image
 */

public interface Jna extends com.sun.jna.Library{

    Jna INSTANCE = (Jna) Native.loadLibrary("./filters/libImageFilters.so", Jna.class);
    
    void cutARGBRed(Pointer imageBytes, int pixelWidth, int pixelHeight);

    void cutARGBGreen(Pointer imageBytes, int pixelWidth, int pixelHeight);

    void cutARGBBlue(Pointer imageBytes, int pixelWidth, int pixelHeight);

    void mirror(Pointer imageBytes, int pixelWidth, int pixelHeight);
    
}
