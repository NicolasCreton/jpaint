
package abstractClass;

import model.ShapeListModel;
import model.ShapeModel;
import model.ToolsModel;

/**
 *
 * Outil de dessin
 * Classe abstraite héritant des outils abstraits
 * Il permet de créer et modifier la shape. Et ainsi de la mettre dans le modele de Shape
 */
public abstract class AbstractDrawingTool extends AbstractTool{
    
    public void createShape(ShapeModel shapeModel, ToolsModel toolsModel, ShapeListModel shapeListModel){
        setShapeModel(shapeModel);
        shapeListModel.addShape(shapeModel);
    }
    
    public void modifyShape(ShapeModel shapeModel, ShapeListModel shapeListModel, ToolsModel toolsModel){
        shapeListModel.deleteShape(getShapeModel());
        setShapeModel(shapeModel);
        shapeListModel.addShape(shapeModel);
    }
}
