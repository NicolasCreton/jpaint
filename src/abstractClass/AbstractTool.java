
package abstractClass;

import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ShapeModel;
import model.ToolsModel;

/**
 * Classe d'outil abstrait
 * Classe mère de tout les outils
 * 
 */
public abstract class AbstractTool extends MouseAdapter{
    private ShapeModel shapeModel;
    private String name;
    private JButton button;


    public ShapeModel getShapeModel() {
        return shapeModel;
    }

    public void setShapeModel(ShapeModel shapeModel) {
        this.shapeModel = shapeModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }
    
    public static void addButtonListener(JButton button, ToolsModel model, AbstractTool tool){
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                model.setTool(tool);
            }
        });
        
    }
    
    
    
    public void modifyShape(Shape shape, ShapeListModel shapeListModel, ToolsModel toolsModel){}
    
}
