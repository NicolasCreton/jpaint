
package abstractClass;

import java.awt.event.MouseEvent;
import model.ShapeListModel;
import model.ShapeModel;
import model.ToolsModel;

/**
 * Outil de manipulation
 * Classe abstraite héritant des outils abstraits
 * Il permet de colorier, selectionner une shape
 */
public abstract class AbstractManipulationTool extends AbstractTool{
    
    public void colorShape(ShapeListModel shapeListModel, ToolsModel toolsModel) {
        shapeListModel.deleteShape(getShapeModel());
        getShapeModel().setColorIn(toolsModel.getColorIn());
        getShapeModel().setColorOut(toolsModel.getColorOut());
        shapeListModel.addShape(getShapeModel());
    }
    
    public void selectShape(ShapeListModel shapeListModel, MouseEvent event){
        for (int i = 0; i < shapeListModel.getShapesList().size(); i++) {
            ShapeModel actual = shapeListModel.getShapesList().get(i);
            if (actual.getShape().getBounds2D().contains(event.getX(), event.getY())) {
                setShapeModel(shapeListModel.getShapesList().get(i));
            }
        }
    }
    
    public void setSelected(Boolean b, ShapeListModel shapeListModel){
        shapeListModel.deleteShape(getShapeModel());
        getShapeModel().setIsSelected(b);
        shapeListModel.addShape(getShapeModel());
    }
    
}
