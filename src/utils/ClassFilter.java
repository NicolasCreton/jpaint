package utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Cet outil permet de filter le contenu des dossiers d'un JFileChooser
 * Ici on ne va récupérer que les fichiers class et les dossiers pour naviguer
 * Il override FileFilter
 */
public class ClassFilter extends FileFilter{
    
    private String acceptedExtensions;

    public ClassFilter() {
        this.acceptedExtensions = "class";
    }

    @Override
    public boolean accept(File file) {
        if (file.getName().toLowerCase().endsWith(acceptedExtensions) || file.isDirectory()) {
            return true;
        } else {
            //System.err.println("Not accepted");
            return false;
        }
    }

    @Override
    public String getDescription() {
        return "Class files";
    }
    
    
    
}
