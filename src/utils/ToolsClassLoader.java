/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import abstractClass.AbstractTool;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ShapeListModel;
import model.ToolsModel;

/**
 *Cette classe permet d'instancier les fichier class déjà compilés dans un dossier
 * Nous vérifions si le fichier fait parti du bon packages et est rajouté dans la liste de tools
 * 
 */
public class ToolsClassLoader{
    
    ShapeListModel shapeListModel;
    ToolsModel toolsModel;

    public ToolsClassLoader(ShapeListModel shapeListModel, ToolsModel toolsModel) {
        super();
        this.shapeListModel = shapeListModel;
        this.toolsModel = toolsModel;
    }
    
    public void addNewTool(File selectedFile) {
        try {
            String fileName = selectedFile.getName().substring(0, selectedFile.getName().lastIndexOf("."));
            String path = selectedFile.getPath().substring(0, selectedFile.getPath().lastIndexOf(File.separator));
            File folder = new File(path);
            URL url = folder.toURI().toURL();
            ClassLoader classLoader = new URLClassLoader(new URL[]{url});
            Class class1 = classLoader.loadClass("controler.Tools." + fileName);
            Object object = class1.getDeclaredConstructor(new Class[]{model.ShapeListModel.class, model.ToolsModel.class}).newInstance(new Object[]{this.shapeListModel, this.toolsModel});
            this.toolsModel.addToolToList((AbstractTool)object);
        } catch (SecurityException | IllegalArgumentException ex) {
        } catch (MalformedURLException | ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(ToolsClassLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
