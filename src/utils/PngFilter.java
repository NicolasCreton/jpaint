/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Cet outil permet de filter le contenu des dossiers d'un JFileChooser
 * Ici on ne va récupérer que les fichiers PNG et les dossiers pour naviguer
 * Il override FileFilter
 */
public class PngFilter extends FileFilter{
    private String acceptedExtensions;

    public PngFilter() {
        this.acceptedExtensions = "png";
    }

    @Override
    public boolean accept(File file) {
        if (file.getName().toLowerCase().endsWith(acceptedExtensions) || file.isDirectory()) {
            return true;
        } else {
            //System.err.println("Not accepted");
            return false;
        }
    }

    @Override
    public String getDescription() {
        return "Png Files";
    }
}
