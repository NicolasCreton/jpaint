
package MyShape;

import java.awt.geom.Rectangle2D;
import model.ShapeModel;
import model.ToolsModel;

/**
 * Rectangle personnalisée héritant du modèle de shape
 * 
 */
public class MyRectangle extends ShapeModel{

    public MyRectangle(int x, int y, int x2, int y2, ToolsModel toolsModel) {
        setShape(new Rectangle2D.Double(x, y, x2, y2));
        setIsSelected(false);
        setColorIn(toolsModel.getColorIn());
        setColorOut(toolsModel.getColorOut());
    }

    @Override
    public void moveShape(int x, int y) {
        setShape(new Rectangle2D.Double(x, y, getShape().getBounds().getWidth() , getShape().getBounds().getHeight()));
    }
    
    
    
}
