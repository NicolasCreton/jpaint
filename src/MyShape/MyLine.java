
package MyShape;

import java.awt.geom.Line2D;
import model.ShapeModel;
import model.ToolsModel;

/**
 *
 * Ligne personnalisée heritant du model de shape
 */
public class MyLine extends ShapeModel{
    
    public MyLine(int x, int y, int x2, int y2, ToolsModel toolsModel) {
        setShape(new Line2D.Double(x, y, x2, y2));
        setIsSelected(false);
        setColorIn(toolsModel.getColorIn());
        setColorOut(toolsModel.getColorOut());
    }

    @Override
    public void moveShape(int x, int y) {
        setShape(new Line2D.Double(x, y, getShape().getBounds2D().getWidth()+ x, getShape().getBounds2D().getHeight()+ y));
    }
    
}
