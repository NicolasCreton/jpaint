
package MyShape;

import java.awt.geom.Ellipse2D;
import model.ShapeModel;
import model.ToolsModel;

/**
 * Ellipse personnalisée héritant du modèle de Shape
 * 
 */
public class MyEllipse extends ShapeModel{
    public MyEllipse(int x, int y, int x2, int y2, ToolsModel toolsModel) {
        setShape(new Ellipse2D.Double(x, y, x2, y2));
        setIsSelected(false);
        setColorIn(toolsModel.getColorIn());
        setColorOut(toolsModel.getColorOut());
    }

    @Override
    public void moveShape(int x, int y) {
        setShape(new Ellipse2D.Double(x, y, getShape().getBounds().getWidth() , getShape().getBounds().getHeight()));
    }
}
