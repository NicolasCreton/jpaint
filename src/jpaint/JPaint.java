/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaint;
import controler.ToolControler;
import model.ShapeListModel;
import model.ToolsModel;
import view.Frame;

/**
 * Point de départ de l'application
 * Instanciation des modèles et des controleurs principaux
 */
public class JPaint {

    
    public static void main(String[] args) {
        Frame vue;
        ToolsModel tools;
        ShapeListModel slm;
        ToolControler toolControler;


        slm = new ShapeListModel();
        tools = new ToolsModel();
        vue = new Frame(slm, tools);
        toolControler = new ToolControler(slm, tools, vue);

    }
}
