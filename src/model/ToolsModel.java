/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import abstractClass.AbstractTool;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Modèle d'outil permettant de stocker l'outil en cours et le précédent (utile lors du changement d'outil)
 * Ce modèle est mis à jour grace aux choix par les différents boutons dans la barre d'outils
 * Il permet aussi d'assigner une couleur lors du dessin d'une forme
 */
public class ToolsModel extends Observable{
    private AbstractTool tool;
    private ArrayList<AbstractTool> toolsList;

    private Color colorIn;
    private Color colorOut;

    public ToolsModel() {
        this.colorIn = Color.WHITE;
        this.colorOut = new Color(0x070E24);
        this.toolsList = new ArrayList();
    }

    public Color getColorIn() {
        return colorIn;
    }

    public void setColorIn(Color colorIn) {
        this.colorIn = colorIn;
        this.setChanged();
        this.notifyObservers();
    }

    public Color getColorOut() {
        return colorOut;
    }

    public void setColorOut(Color colorOut) {
        this.colorOut = colorOut;
        this.setChanged();
        this.notifyObservers();
    }

    public String getActualToolName(){
        try {
            return this.tool.getName();
        } catch (NullPointerException e) {
            return "No";
        }
    }

    public AbstractTool getTool() {
        return tool;
    }

    public void setTool(AbstractTool tool) {
        this.tool = tool;
        setChanged();
        notifyObservers();
    }
    
    public void addToolToList(AbstractTool tool){
        this.tool = tool;
        this.toolsList.add(tool);
        setChanged();
        notifyObservers();
    }
    
    public void deleteToolinList(){
        this.toolsList.remove(getTool());
        this.tool = null;
        setChanged();
        notifyObservers();
    }

    public ArrayList<AbstractTool> getToolsList() {
        return toolsList;
    }
    
    
    
    
    
    
}
