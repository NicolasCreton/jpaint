package model;

import java.awt.Color;
import java.awt.Shape;


/**
 * Modèle de Shape
 * Classe abstraite permettant de définir notre modèle de shape, stocké ensuite dans la liste prévue à cet effet.
 * Ici nous avons une Shape ainsi que sa couleur
 * Mais aussi un booléen de séléction et la couleur précédente de la Shape, ce qui nous permet de la remettre d'orogine après la séléction (Changment de couleur en bleu)
 */
public abstract class ShapeModel {
    private Shape shape;
    private Color colorIn;
    private Color colorOut;
    private Color previousColorIn;
    private Color previousColorOut;
    private Boolean isSelected;


    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Color getColorIn() {
        return colorIn;
    }

    public void setColorIn(Color colorIn) {
        this.colorIn = colorIn;
        this.previousColorIn = colorIn;
    }

    public Color getColorOut() {
        return colorOut;
    }

    public void setColorOut(Color colorOut) {
        this.colorOut = colorOut;
    }

    public Color getPreviousColor() {
        return previousColorIn;
    }

    public void setPreviousColor(Color previousColor) {
        this.previousColorIn = previousColor;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    /**
     * Si la forme est séléctionnée, elle devient bleue
     * @param isSelected
     */
    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
        if (isSelected == true){
            this.previousColorIn = this.colorIn;
            this.colorIn =Color.blue;
            this.previousColorOut = this.colorOut;
            this.colorOut = Color.BLUE;
        }else{
            this.colorIn = previousColorIn;
            this.colorOut = previousColorOut;
        }

    }
    
    public void moveShape(int x, int y){
        
    }
}
