package model;

import java.util.ArrayList;
import java.util.Observable;


/**
 * Modèle de stockage de modèle de Shape
 * Les ShapeModel sont stockées dans une Arraylist
 */
public class ShapeListModel extends Observable{
    private ArrayList<ShapeModel> shapesList;

    public ShapeListModel() {
        this.shapesList = new ArrayList<ShapeModel>();
    }

    public void addShape(ShapeModel currentShape) {
        this.shapesList.add(currentShape);
        this.setChanged();
        this.notifyObservers();
    }

    public void deleteShape(ShapeModel toDelete) {
        this.shapesList.remove(toDelete);
        this.setChanged();
        this.notifyObservers();
    }

    public ArrayList<ShapeModel> getShapesList() {
        return shapesList;
    }
}
