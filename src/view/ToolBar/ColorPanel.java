package view.ToolBar;

import javax.swing.JPanel;
import model.ToolsModel;

/**
 * Panel comprenant les deux boutons de choix des couleurs
 * Ainsi ils restent toujours cote à cote ne cas de changement d'orientation de la barre d'outils
 * 
 */
public class ColorPanel extends JPanel{

    public ColorPanel(ToolsModel toolsModel) {
        
        ColorButton colorIn = new ColorButton(" ", toolsModel, "in");
        colorIn.setFocusable(false);
        ColorButton colorOut = new ColorButton(" ", toolsModel, "out");
        colorOut.setFocusable(false);
        
        this.add(colorIn);
        this.add(colorOut);
    }
}
