package view.ToolBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ToolsModel;

/**
 * Bouton permettant de supprimer une classe chargé par introspection
 * Il supprime donc l'outil courant de la liste d'outils
 * 
 */
public class DeleteClassButton extends JButton{
    

    public DeleteClassButton(ImageIcon img,ToolsModel toolsModel){
        super(img);
        
        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolsModel.deleteToolinList();
            }
        });
        
        this.setToolTipText("Delete Plugin");
        this.setAlignmentX(CENTER_ALIGNMENT);
        this.setAlignmentY(CENTER_ALIGNMENT);
    }
}
