/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.ToolBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import model.ShapeListModel;
import model.ToolsModel;
import utils.ClassFilter;
import utils.ToolsClassLoader;

/**
 * Ce bouton permet d'ajouter des classes grace à un FileChooser qui va filter que les fichiers class
 * 
 */
public class AddClassButton extends JButton{
    private ToolsClassLoader toolsClassLoader;
    

    public AddClassButton(ShapeListModel shapeListModel, ToolsModel toolsModel, ImageIcon img){
        super(img);
        this.toolsClassLoader = new ToolsClassLoader(shapeListModel, toolsModel);
        
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
            File folder = new File("./tools");
            ClassFilter classFilter = new ClassFilter();
            JFileChooser fileChooser = new JFileChooser(folder);
            fileChooser.setDialogTitle("Choisir une classe");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileFilter(classFilter);
            int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    toolsClassLoader.addNewTool(selectedFile);
                }
            }
        });
        this.setToolTipText("Add Plugin");
        this.setAlignmentX(CENTER_ALIGNMENT);
        this.setAlignmentY(CENTER_ALIGNMENT);
    }
}
