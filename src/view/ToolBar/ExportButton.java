package view.ToolBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import jna.JnaExporter;
import view.DrawSpace;

/**
 * Bouton d'exportation de l'image
 * Il est instancié 5 fois afin d'exporter l'image dans la couleur voulue (Red - Blue - Green) ou afin de faire un mirror ou de garder l'original
 * 
 */
public class ExportButton extends JButton{
    
    private JnaExporter jnaExporter;

    public ExportButton(String title,DrawSpace drawSpace) {
        super(title);
        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                jnaExporter = new JnaExporter(drawSpace, title);
            }
        });
    }
    
    
    
    
}
