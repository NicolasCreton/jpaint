package view.ToolBar;

import Listeners.CancelActionListener;
import Listeners.OkActionListener;
import Listeners.OpenColorChooser;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import model.ToolsModel;

/**
 * Bouton de choix des couleurs il va charger la couleur choisi par le ColorChooser dans le modèle et mettre à jour la couleur de son bouton
 * Ce bouton est instancié deux fois, pour In et pour Out
 * 
 */
public class ColorButton extends JButton implements Observer{
    private ToolsModel toolsModel;
    private String inOrOut;
    
    public ColorButton(String text, final ToolsModel toolsModel, String inOrOut) {
        super(text);
        this.inOrOut =inOrOut;
        this.toolsModel = toolsModel;
        this.toolsModel.addObserver(this);
        if (inOrOut.equals("in")) {
            this.setBackground(this.toolsModel.getColorIn());
        } else {
            this.setBackground(this.toolsModel.getColorOut());
        }
        JColorChooser colorChooser = new JColorChooser();
        OkActionListener okActionListener = new OkActionListener(toolsModel, inOrOut, colorChooser);
        CancelActionListener cancelActionListener = new CancelActionListener(toolsModel, inOrOut, colorChooser);
        this.addActionListener(new OpenColorChooser(colorChooser, okActionListener, cancelActionListener));
        if (inOrOut.equals("in")) {
            this.setToolTipText("Color in Shape");
        } else {
            this.setToolTipText("Color out Shape");
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (this.inOrOut.equals("in")) {
            this.setBackground(this.toolsModel.getColorIn());
        } else {
            this.setBackground(this.toolsModel.getColorOut());
        }
    }
}
