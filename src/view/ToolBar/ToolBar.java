package view.ToolBar;

import abstractClass.AbstractDrawingTool;
import abstractClass.AbstractManipulationTool;
import abstractClass.AbstractTool;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import model.ShapeListModel;
import model.ToolsModel;
import view.Frame;


/**
 * Barre d'outil détachable contenant les différents outils disponinles dans l'application
 * Elle s'oriente automatiquement grace à l'utilisation d'une JToolbar
 */
public class ToolBar extends JToolBar implements Observer{
   private ColorPanel colorPanel;
   
   private AddClassButton addClassButton;
   private DeleteClassButton deleteClassButton;
   private ExportButton exportButtonOK;
   private HelpButton helpButton;
   
   private JPanel panelDrawingTools;
   private JPanel panelManipulationTools;
   
   private ToolsModel toolsModel;
   private ShapeListModel shapeListModel;
   
   
   
   public ToolBar(ShapeListModel shapeListModel, ToolsModel toolsModel, Frame frame){
        super();
        this.toolsModel = toolsModel;
        this.shapeListModel = shapeListModel;
        
        this.panelManipulationTools = new JPanel();
        this.add(this.panelManipulationTools);
        
        this.addSeparator();
        
        this.panelDrawingTools = new JPanel();
        this.add(this.panelDrawingTools);
        
        this.addSeparator();
        
        this.colorPanel = new ColorPanel(this.toolsModel);
        
        ImageIcon addClassImg = new ImageIcon("./src/ressource/addClass.png");
        this.addClassButton = new AddClassButton(this.shapeListModel, this.toolsModel, addClassImg);
        this.addClassButton.setFocusable(false);
        
        ImageIcon deleteClassImg = new ImageIcon("./src/ressource/deleteClass.png");
        this.deleteClassButton = new DeleteClassButton(deleteClassImg, this.toolsModel);
        this.deleteClassButton.setFocusable(false);
        
        ImageIcon helpImg = new ImageIcon("./src/ressource/help.png");
        this.helpButton = new HelpButton(helpImg);
        this.helpButton.setFocusable(false);
        
        
        this.exportButtonOK = new ExportButton("blue", frame.getDrawSpace());
        this.add(this.exportButtonOK);
        this.exportButtonOK.setFocusable(false);
        
        this.exportButtonOK = new ExportButton("red", frame.getDrawSpace());
        this.add(this.exportButtonOK);
        this.exportButtonOK.setFocusable(false);
        
        this.exportButtonOK = new ExportButton("green", frame.getDrawSpace());
        this.add(this.exportButtonOK);
        this.exportButtonOK.setFocusable(false);
        
        this.exportButtonOK = new ExportButton("mirror", frame.getDrawSpace());
        this.add(this.exportButtonOK);
        this.exportButtonOK.setFocusable(false);
        
        this.exportButtonOK = new ExportButton("original", frame.getDrawSpace());
        this.add(this.exportButtonOK);
        this.exportButtonOK.setFocusable(false);
        
        this.add(colorPanel);
        
        this.add(this.addClassButton);
        this.add(this.deleteClassButton);
        this.add(this.helpButton);
        
        this.toolsModel.addObserver(this);
        
        
   }

    @Override
    public void update(Observable o, Object arg) {
        this.panelDrawingTools.removeAll();
        this.panelManipulationTools.removeAll();
        for (AbstractTool tool : this.toolsModel.getToolsList()) {
            if (tool instanceof AbstractDrawingTool) {
                tool.addButtonListener(tool.getButton(), this.toolsModel, tool);
                this.panelDrawingTools.add(tool.getButton());
            }else if(tool instanceof AbstractManipulationTool){
                tool.addButtonListener(tool.getButton(), this.toolsModel, tool);
                this.panelManipulationTools.add(tool.getButton());
            }
        }
    }
}
