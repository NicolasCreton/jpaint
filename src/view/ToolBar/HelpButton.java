/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.ToolBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author nicolas
 */
public class HelpButton extends JButton{

    public HelpButton(ImageIcon imageIcon) {
        super(imageIcon);
        
        this.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Click on a shape to select it, click on another to select it too. Del to delete it and Esc to deselect it ","Help", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }
    
    
    
}
