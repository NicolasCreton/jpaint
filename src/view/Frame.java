/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import model.ShapeListModel;
import model.ToolsModel;
import view.ToolBar.ToolBar;

/**
 * Frame principale du Paint
 * Appelle le repaint à chaque changement dans la liste de ShapeModel
 */

public class Frame extends JFrame implements Observer{
    private ShapeListModel shapelist;
    private DrawSpace drawSpace;
    public StatusBar statuBar;
    public ToolBar toolBar;

    public Frame(ShapeListModel slm, ToolsModel toolsModel) {
        this.setLayout(new BorderLayout(0, 0));
        this.setTitle("JPaint");

        this.shapelist = slm;
        slm.addObserver(this);
        this.drawSpace = new DrawSpace(slm);
        this.add(drawSpace, BorderLayout.CENTER);
        this.requestFocus();
        
        this.toolBar = new ToolBar(shapelist, toolsModel, this);
        this.toolBar.setFocusable(false);
        this.add(this.toolBar, BorderLayout.NORTH);
        
        
        this.statuBar = new StatusBar(toolsModel, this);
        this.statuBar.setFocusable(false);
        this.add(this.statuBar, BorderLayout.SOUTH);

        this.setExtendedState(MAXIMIZED_BOTH);
        this.setFocusable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    public DrawSpace getDrawSpace() {
        return drawSpace;
    }

    public StatusBar getStatuBar() {
        return statuBar;
    }

    public ToolBar getToolBar() {
        return toolBar;
    }
    
    @Override
    public void update(Observable o, Object arg) {
        this.drawSpace.repaint();
    }
}
