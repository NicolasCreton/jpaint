/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import model.ToolsModel;

/**
 * Barre de statut indiquant l'outil en cours d'utilisation
 */
public class StatusBar extends JLabel {
    private ToolsModel toolsModel;
    private Frame frame;
    
    public StatusBar(ToolsModel toolsModel, Frame frame) {
        super();
        this.toolsModel = toolsModel;
        this.frame = frame;
        this.setBorder(BorderFactory.createLineBorder(Color.gray));
    }
    
    public void setToolUsed(){
        if (frame.getMouseListeners().length == 0) {
            this.setText(" ["
                    + this.toolsModel.getActualToolName()
                    + "] tool selected");
        } 
            
    }
    
}
