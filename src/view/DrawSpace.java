/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import javax.swing.JPanel;
import model.ShapeListModel;

/**
 * Panel de dessin des formes
 * Il dessine les formes dans la liste à chaque changement dans la liste de ShapeModel
 * La couleur des formes changent en fonction de celle stockée dans le modèle de ShapeModel
 */
public class DrawSpace extends JPanel{

    private ShapeListModel slm;
    private Graphics2D g2d;

    public DrawSpace(ShapeListModel slm) {
        super();
        this.slm = slm;
        this.setBackground(Color.WHITE);
    }

    @Override
    public void paintComponent ( Graphics g )
    {
        super.paintComponent(g);
        this.g2d = (Graphics2D) g;
        try {
            for (int i = 0; i < this.slm.getShapesList().size(); i++) {
                Shape shape = this.slm.getShapesList().get(i).getShape();
                g2d.setColor(this.slm.getShapesList().get(i).getColorIn());
                g2d.fill(shape);
                g2d.setColor(this.slm.getShapesList().get(i).getColorOut());
                g2d.draw(shape);
            }
        } catch (NullPointerException e) {
            //Do nothing
        }
    }

    public Graphics2D getG2d() {
        return g2d;
    }
    
    
    
}
