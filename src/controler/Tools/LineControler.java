package controler.Tools;

import MyShape.MyLine;
import abstractClass.AbstractDrawingTool;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ToolsModel;

/**
 * Controleur permettant de créer des MyLine
 */
public class LineControler extends AbstractDrawingTool {
    private ShapeListModel shapeListModel;
    private ToolsModel toolsModel;
    private int originalX;
    private int originalY;
    private MyLine line2D;

    public LineControler(ShapeListModel shapeListModel, ToolsModel toolsModel) {
        this.shapeListModel = shapeListModel;
        this.toolsModel = toolsModel;
        setName("Line");
        setButton(new JButton());
        getButton().setIcon(new ImageIcon("./src/ressource/line.png"));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.line2D = new MyLine(e.getX(), e.getY(), e.getX(), e.getY(), this.toolsModel);
        this.originalX = (int)this.line2D.getShape().getBounds2D().getX();
        this.originalY = (int) this.line2D.getShape().getBounds2D().getY();
        createShape(line2D, this.toolsModel, this.shapeListModel);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.line2D = new MyLine( this.originalX, this.originalY, e.getX(), e.getY(), this.toolsModel);
        modifyShape(line2D, this.shapeListModel, this.toolsModel);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.line2D = new MyLine( this.originalX, this.originalY, e.getX(), e.getY(), this.toolsModel);
        modifyShape(line2D, this.shapeListModel, this.toolsModel);
    }
}
