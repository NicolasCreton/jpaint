package controler.Tools;

import MyShape.MyRectangle;
import abstractClass.AbstractDrawingTool;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ToolsModel;

/**
 * Controleur permettant de créer des MyRectangle
 */
public class RectangleControler extends AbstractDrawingTool {
    private ShapeListModel slm;
    private ToolsModel toolsModel;
    private MyRectangle rectangle2D;
    private int originalX;
    private int originalY;

    public RectangleControler(ShapeListModel slm, ToolsModel toolsModel) {
        this.slm = slm;
        this.toolsModel = toolsModel;
        setName("Rectangle");
        setButton(new JButton());
        getButton().setIcon(new ImageIcon("./src/ressource/rect.png"));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.rectangle2D = new MyRectangle(e.getX(), e.getY(),0,0, this.toolsModel);
        this.originalX = (int) this.rectangle2D.getShape().getBounds2D().getX();
        this.originalY = (int) this.rectangle2D.getShape().getBounds2D().getY();
        createShape(this.rectangle2D, this.toolsModel, this.slm);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
       this.rectangle2D = makeRectangle(this.originalX, this.originalY, e.getX(), e.getY());
       modifyShape(this.rectangle2D, this.slm, this.toolsModel);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.rectangle2D = makeRectangle(this.originalX, this.originalY, e.getX(), e.getY());
        modifyShape(this.rectangle2D, this.slm, this.toolsModel);
        
    }

    public MyRectangle makeRectangle(int x1, int y1, int x2, int y2) {
        
        int x = Math.min(x1, x2);
        int w = Math.abs(x2 - x1);

        int y = Math.min(y1, y2);
        int h = Math.abs(y2 - y1);
        return new MyRectangle(x, y, w, h, this.toolsModel);
    }
}
