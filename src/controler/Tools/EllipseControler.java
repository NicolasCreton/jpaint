package controler.Tools;

import MyShape.MyEllipse;
import abstractClass.AbstractDrawingTool;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ToolsModel;

/**
 * Controleur permettant de créer des MyEllipse
 */
public class EllipseControler extends AbstractDrawingTool {
    private ShapeListModel slm;
    private ToolsModel toolsModel;
    private int originalX;
    private int originalY;
    private MyEllipse ellipse2D;

    public EllipseControler(ShapeListModel slm, ToolsModel toolsModel) {
        this.slm = slm;
        this.toolsModel = toolsModel;
        setName("Ellipse");
        setButton(new JButton());
        getButton().setIcon(new ImageIcon("./src/ressource/oval.png"));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.ellipse2D = new MyEllipse(e.getX(), e.getY(), 0, 0, this.toolsModel);
        this.originalX = (int) this.ellipse2D.getShape().getBounds().getX();
        this.originalY = (int) this.ellipse2D.getShape().getBounds().getY();
        createShape(ellipse2D, this.toolsModel, this.slm);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int clickX = e.getX();
        int clickY = e.getY();        
        this.ellipse2D =  new MyEllipse(Math.min(this.originalX, clickX), Math.min(this.originalY, clickY), Math.abs(this.originalX - clickX), Math.abs(this.originalY - clickY), this.toolsModel);
        modifyShape(ellipse2D, this.slm, this.toolsModel);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int clickX = e.getX();
        int clickY = e.getY();        
        this.ellipse2D =  new MyEllipse(Math.min(this.originalX, clickX), Math.min(this.originalY, clickY), Math.abs(this.originalX - clickX), Math.abs(this.originalY - clickY), this.toolsModel);
        modifyShape(ellipse2D, this.slm, this.toolsModel);
    }
}
