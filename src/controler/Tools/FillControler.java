package controler.Tools;

import abstractClass.AbstractManipulationTool;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ToolsModel;

/**
 * Controleur permettant de remplir la shape avec les couleurs définies dans le modèle
 * Il va changer les champs de couleurs dans le modèle de shape
 */
public class FillControler extends AbstractManipulationTool {

    private ShapeListModel shapeListModel;
    private ToolsModel toolsModel;

    public FillControler(ShapeListModel shapeListModel, ToolsModel toolsModel) {
        this.toolsModel = toolsModel;
        this.shapeListModel = shapeListModel;
        setName("Fill");
        setButton(new JButton());
        getButton().setIcon(new ImageIcon("./src/ressource/fill.png"));
    }

    
    @Override
    public void mousePressed(MouseEvent e) {
        try {
            selectShape(this.shapeListModel, e);
            colorShape(this.shapeListModel, this.toolsModel);
        } catch (NullPointerException ne) {}
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {

    }
}
