package controler.Tools;

import abstractClass.AbstractManipulationTool;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.ShapeListModel;
import model.ToolsModel;

/**
 * Controleur permettant de sélectionner une forme en fonction du clik dans la zone de dessin
 */

public class SelectControler extends AbstractManipulationTool {
    private ShapeListModel shapeListModel;
    
    public SelectControler(ShapeListModel shapeListModel, ToolsModel toolsModel) {
        this.shapeListModel = shapeListModel;
        setName("Select");
        setButton(new JButton());
        getButton().setIcon(new ImageIcon("./src/ressource/select.png"));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        try {
            selectShape(this.shapeListModel, e);
            if (getShapeModel().getIsSelected() == false) {
                setSelected(true, this.shapeListModel);
            }
        } catch (NullPointerException e1) {
        }
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        try {
            shapeListModel.deleteShape(getShapeModel());
            getShapeModel().moveShape(e.getX(), e.getY());
            shapeListModel.addShape(getShapeModel());
        } catch (NullPointerException ne) {
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        try {
            //On repasse le shapeModelactuel à null afin d'éviter qu'il ne réaparaisse après suppréssion ou désélection
            setShapeModel(null);
        } catch (NullPointerException ne) {
        }
    }

}
