package controler;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import model.ShapeListModel;
import model.ShapeModel;


/**
 * Controleur permettant de supprimer une forme en appuyant sur SUPPR
 * Supprime la forme de la liste de Shape
 */
public class DeleteControler implements KeyListener {

    private ShapeListModel shapeListModel;

    public DeleteControler(ShapeListModel shapeListModel) {
        this.shapeListModel = shapeListModel;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            for (int i = 0; i < this.shapeListModel.getShapesList().size(); i++) {
                if (this.shapeListModel.getShapesList().get(i).getIsSelected() == true) {
                    ShapeModel shapeModel = this.shapeListModel.getShapesList().get(i);
                    this.shapeListModel.deleteShape(shapeModel);
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
