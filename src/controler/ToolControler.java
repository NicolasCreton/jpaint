/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.util.Observable;
import java.util.Observer;
import model.ShapeListModel;
import model.ToolsModel;
import view.Frame;

/**
 * Controleur permettant d'instancier les différents oultils et de les switcher en fonction de celui séléctionné dans le modèle
 * Ce controleur est mis à jour par ToolsModel lors d'un changement d'outils
 */
public class ToolControler implements Observer {

    private ShapeListModel shapeListModel;
    private DeleteControler deleteControler;
    private DeselectControler deselectControler;
    private Frame vue;
    private ToolsModel tools;


    public ToolControler(ShapeListModel shapeListModel, ToolsModel tools, Frame vue) {
        this.tools = tools;
        this.vue = vue;
        this.shapeListModel = shapeListModel;

        this.deleteControler = new DeleteControler(this.shapeListModel);
        this.deselectControler = new DeselectControler(this.shapeListModel);

        this.vue.addKeyListener(this.deleteControler);
        this.vue.addKeyListener(this.deselectControler);

        this.tools.addObserver(this);
    }


    @Override
    public void update(Observable o, Object arg) {
        
        //Ici nous vérifions si il n'y a pas déjà un controleur de selectionné, si oui, on le supprime et on le remplace
        deleteListeners();

        this.vue.getDrawSpace().addMouseListener(this.tools.getTool());
        this.vue.getDrawSpace().addMouseMotionListener(this.tools.getTool());
        
        this.vue.statuBar.setToolUsed();
    }
    
    public void deleteListeners(){
        try {
                this.vue.getDrawSpace().removeMouseListener(this.vue.getDrawSpace().getMouseListeners()[0]);
                this.vue.getDrawSpace().removeMouseMotionListener(this.vue.getDrawSpace().getMouseMotionListeners()[0]);
            } catch (ArrayIndexOutOfBoundsException exception) {
                //Do nothing
            }
    }
}
