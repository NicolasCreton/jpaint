package controler;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import model.ShapeListModel;
import model.ShapeModel;


/**
 * Controleur permettant de désélectionner une forme en appuyant sur entrée
 * Il passe le booleen de selection à false
 */
public class DeselectControler implements KeyListener{

    private ShapeListModel shapeListModel;

    public DeselectControler(ShapeListModel shapeListModel) {
        this.shapeListModel = shapeListModel;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            try {
                for (int i = 0; i < this.shapeListModel.getShapesList().size(); i++) {
                    if (this.shapeListModel.getShapesList().get(i).getIsSelected() == true) {
                        ShapeModel shapeModel = this.shapeListModel.getShapesList().get(i);
                        this.shapeListModel.deleteShape(shapeModel);
                        shapeModel.setIsSelected(false);
                        this.shapeListModel.addShape(shapeModel);
                    }
                }
            } catch (IndexOutOfBoundsException e1) {
                //e1.printStackTrace();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
