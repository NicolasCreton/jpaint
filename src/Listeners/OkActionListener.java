
package Listeners;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JColorChooser;
import model.ToolsModel;

/**
 *Boutons d'approbation du choix de couleur
 * La couleur choisie est assignée à color In ou Out
 * 
 */
public class OkActionListener implements ActionListener{
    private ToolsModel toolsModel;
    private String inOrOut;
    private JColorChooser chooser;

    public OkActionListener(ToolsModel toolsModel, String inOrOut, JColorChooser chooser) {
        this.toolsModel = toolsModel;
        this.inOrOut = inOrOut;
        this.chooser = chooser;
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.inOrOut.equals("in")) {
            Color color = this.chooser.getColor();
            this.toolsModel.setColorIn(color);
        } else {
            Color color = this.chooser.getColor();
            this.toolsModel.setColorOut(color);
        }
    }

    
}
