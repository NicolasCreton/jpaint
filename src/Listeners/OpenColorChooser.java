
package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JColorChooser;
import javax.swing.JDialog;

/**
 *JcolorChooser permettant de choisir la couleur interieur ou exterieure
 * 
 */
public class OpenColorChooser implements ActionListener{
    
    private JColorChooser colorChooser;
    private OkActionListener okActionListener;
    private CancelActionListener cancelActionListener;

    public OpenColorChooser(JColorChooser colorChooser, OkActionListener okActionListener, CancelActionListener cancelActionListener) {
        this.colorChooser = colorChooser;
        this.okActionListener = okActionListener;
        this.cancelActionListener = cancelActionListener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog dialog = JColorChooser.createDialog(null, "Change color", true, this.colorChooser, this.okActionListener, this.cancelActionListener);
        dialog.setVisible(true);
    }
    
    

    
    
    
}
