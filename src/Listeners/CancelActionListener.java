
package Listeners;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JColorChooser;
import model.ToolsModel;

/**
 * *
 * Listener de bouton d'annulation du choix de la couleur
 * Si on annule, la couleur choisie devient noir
 */
public class CancelActionListener implements ActionListener{
    
    private ToolsModel toolsModel;
    private String inOrOut;

    public CancelActionListener(ToolsModel toolsModel, String inOrOut, JColorChooser chooser) {
        this.toolsModel = toolsModel;
        this.inOrOut = inOrOut;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.inOrOut.equals("in")) {
            this.toolsModel.setColorIn(Color.BLACK);
        }else{
            this.toolsModel.setColorOut(Color.BLACK);
        }
    }
    
    
    
}
